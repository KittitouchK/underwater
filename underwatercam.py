import tensorflow as tf
import tensorflow_hub as hub
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

import numpy as np
import time
import os
# os.environ["CUDA_VISIBLE_DEVICES"]="0"

from moviepy.video.io.VideoFileClip import VideoFileClip
import cv2
from PIL import Image
from imutils.video import FPS
from imutils.video import FileVideoStream
import imutils
import yaml

os.environ["TFHUB_CACHE_DIR"] = '/tmp/tfhub'
tf_device='/gpu:0'
print(tf.test.is_gpu_available())
print("The following GPU devices are available: %s" % tf.test.gpu_device_name())

#------------------------------params.yaml-----------------------------------
with open("params.yaml") as param:
    data = yaml.load(param, Loader=yaml.FullLoader)

mammal_score = data['mammal_score']
turtle_score = data['turtle_score']
rays_score = data['rays_score']
shark_score = data['shark_score']
fish_score = data['fish_score']

maxboxes = data['max_boxes']
allow_fishes = data['allow_fishes']
seccond = data['sec']
min_video = data['min_video']
norm = data['norm']
#------------------------------------------------------------------------------
def detecting(frame , f , cap_fps , MARINE_MAMMAL , TURTLE , RAY , SHARK , FISH):
    max_boxes = maxboxes 
    allowed_class = [bytes('Marine mammal','utf8') ,bytes('Turtle','utf8') 
                      ,bytes('Rays and skates','utf8'),bytes('Shark','utf8')
                      ,bytes('Fish','utf8')]
    count = []
    animal_list = ['Marine mammal','Turtle','Rays and skates','Shark','Fish']
    set_animal = set(animal_list)
    
    converted_img  = tf.image.convert_image_dtype(frame, tf.float32)[tf.newaxis, ...]
    result = detector(converted_img)
    result = {key:value.numpy() for key,value in result.items()}
    
    boxes, class_names, scores = (result["detection_boxes"], result["detection_class_entities"]
                                  , result["detection_scores"])

    sec = int(f / cap_fps)
    #---------------------------------#---------------------------------#---------------------------------#---------------------------------
    for i in range(min(boxes.shape[0], max_boxes)):
        if class_names[i] == bytes('Marine mammal','utf8') and scores[i] >= mammal_score:
            count.append(class_names[i].decode("ascii"))
        elif class_names[i] == bytes('Turtle','utf8') and scores[i] >= turtle_score:
            count.append(class_names[i].decode("ascii"))
        elif class_names[i] == bytes('Rays and skates','utf8') and scores[i] >= rays_score:
            count.append(class_names[i].decode("ascii"))
        elif class_names[i] == bytes('Shark','utf8') and scores[i] >= shark_score:
            count.append(class_names[i].decode("ascii"))
        elif class_names[i] == bytes('Fish','utf8') and scores[i] >= fish_score:
            count.append(class_names[i].decode("ascii"))
        else:
            continue
    #---------------------------------#---------------------------------#---------------------------------#---------------------------------
    if len(count) == 0:
        return
    else:
        window['-MULTILINE-'].update(window['-MULTILINE-'].get() + "Sec: " + str(sec))
        for animal in set_animal:
            if count.count(animal) == 0:
                continue
            else:
                if 'Marine mammal' in count:
                    MARINE_MAMMAL.append(sec)
                elif 'Turtle' in count:
                    TURTLE.append(sec)
                elif 'Rays and skates' in count:
                    RAY.append(sec)
                elif 'Shark' in count:
                    SHARK.append(sec)                
                elif 'Fish' in count and count.count('Fish') > allow_fishes:
                    FISH.append(sec)  
                window['-MULTILINE-'].update(window['-MULTILINE-'].get() + "Found: " +animal+"  "+str(count.count(animal)))
        window['-MULTILINE-'].update(window['-MULTILINE-'].get() + "-------------------------------------")

def detect_video(filename, vd , sec = 1, skipframe = True):
    cap = cv2.VideoCapture(filename)
    cap_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) 
    cap_fps = int(cap.get(cv2.CAP_PROP_FPS)) 
    execute_sec = sec * cap_fps
    print("Total frame = : {:d}".format(cap_frame))

    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    print("W H = : {:d} {:d}".format(width,height))

    fps = FPS().start()
    if (cap.isOpened() == False):
        print('Error while trying to read video. Please check path again')
        
#------------------------------------------ Loop Frames ---------------------------------------------
    f = 0
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            if (skipframe == True) & (f % execute_sec == 0):
                detecting(frame , f , cap_fps , MARINE_MAMMAL , TURTLE , RAY , SHARK , FISH)
            if (skipframe == False):
                detecting(frame, f , cap_fps , MARINE_MAMMAL , TURTLE , RAY , SHARK , FISH)
            fps.update()
            f += 1
            progress_bar.update_bar(f,cap_frame)
            window["-%-"].update(str(round(f*100/cap_frame)) + " % ")
        else:
            break
#------------------------------------------ End Video -----------------------------------------------
    fps.stop()
    print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
    print("[Video] total. Frames: {:.2f}".format(cap_frame))
    print("[Video] fps: {:.2f}".format(cap_fps)) 
    cap.release()

def ranges_remove_duplicate(nums,norm):
    if nums == []:
        pass
    nums = sorted(set(nums))
    gaps = [[s, e] for s, e in zip(nums, nums[1:]) if s+norm < e]
    edges = iter(nums[:1] + sum(gaps, []) + nums[-1:])
    _list_ = list(zip(edges, edges))
    somelist = [x for x in _list_ if x[1] - x[0] > min_video]
    print(somelist)
    return somelist

def cut(list_,vd,animal,child):
    if list_ == []:
    	return
    vid = VideoFileClip(vd)
    vd_duration = int(vid.duration)
    print(animal)

    if animal == "FISH" and (list_[-1][-1] - list_[0][0]) > (vd_duration/2):
    	clip = vid.subclip(list_[0][0],list_[0][0] + 600)
    	name = animal + str(list_[0][0]) +"_"+ str(list_[0][0] + 600) + ".mp4"
    	cut = clip.write_videofile(child+"/"+name)
    else:
    	for i in list_:
    		a,b = int(i[0]) , int(i[1])
    		if b > vd_duration:
    			b = vd_duration
    		clip = vid.subclip(a,b)
    		name = animal + str(i[0])+"_"+ str(i[1])+".mp4"
    		cut = clip.write_videofile(child+"/"+name)

import PySimpleGUI as sg
import os.path
sg.theme('Dark Blue 3')
#------------------------------------------ First Col -----------------------------------------------
file_list_column = [
    [
        sg.Text("Video Folder : "),
        sg.In(size=(31, 1), enable_events=True, key="-FOLDERIN-"),
        sg.FolderBrowse(),
    ],
    [
        sg.Listbox(values=[], enable_events=True, size=(52, 20), key="-FILE LIST-"),
    ],
]
#------------------------------------------ Second Col ----------------------------------------------
second_column = [
    [
        sg.Text("Output Folder :"),
        sg.In(size=(33, 1), enable_events=True, key="-FOLDEROUT-"),
        sg.FolderBrowse(),
    ],
    [
        sg.Text("Model Status: "),
        sg.Text(size=(25, 1), key="-MODELSTATUS-"),
        sg.Button(('Import Model'), enable_events=True, key="-IMPORTMODEL-"),
    ],
    [
        sg.Text("Process :"),
        sg.Text(size=(34, 1), key="-TOUT-"),
        sg.Button((' Run '), enable_events=True, key="-RUN-"),
    ],
    [
        sg.MLine(size=(47,15), key='-MULTILINE-',do_not_clear=True,autoscroll=True),
        
    ],
    [
        sg.ProgressBar(1, orientation='h', size=(27, 15), key='progress'),
        sg.Text(size=(4, 1), key="-%-"),
    ],
]
#------------------------------------------ Layout --------------------------------------------------
layout = [
    [
        sg.Column(file_list_column),
        sg.Column(second_column),
    ]
]
#------------------------------------------ While loop ----------------------------------------------
window = sg.Window("Underwater CAM v0.1.0", layout)
fnames = []
outfol = []
detector = []
progress_bar = window['progress']
while True:
    event, values = window.read()
    if event == "Exit" or event == sg.WIN_CLOSED:
        break
    elif detector == []:
        window["-MODELSTATUS-"].update(" Unavailable Please Import -->")
    if event == "-FOLDERIN-":
        folder = values["-FOLDERIN-"]
        try:
            file_list = os.listdir(folder)
        except:
            file_list = []
        print(file_list)
        fnames = [
            f
            for f in file_list
            if os.path.isfile(os.path.join(folder, f))
            and f.lower().endswith(".mp4")
        ]
        window["-FILE LIST-"].update(fnames)
        
    elif event == "-FOLDEROUT-":
        try:
            outfol = values["-FOLDEROUT-"]
        except:
            outfol = []

    elif event == "-IMPORTMODEL-":    
        window["-MODELSTATUS-"].update("Importing...")
        print("Import model")
        sg.popup('PRESS OK and Wait until the status -Available-')
        module_handle = "_MODEL_"
        detector = hub.load(module_handle).signatures['default']
        sg.popup('MODEL -Available-')
        window["-MODELSTATUS-"].update("Available")

    elif event == "-RUN-":
        if len(fnames) == 0:
            sg.popup("Please select Video Folder")
        elif outfol == []:
            sg.popup("Please select Output Folder")
        elif detector == []:
            sg.popup("Please import model")
        else:
            try:
                total_video = len(fnames)
                if sg.popup_ok_cancel('Total Video: '+ str(total_video)) == "OK":
                    for vd in fnames:
                        MARINE_MAMMAL = []
                        TURTLE = []
                        RAY = []
                        SHARK = []
                        FISH = []
                        window["-TOUT-"].update("Operating on " + str(vd))
                        window['-MULTILINE-'].update(window['-MULTILINE-'].get() + "Operating on " + str(vd))
                        detect_video(os.path.join(folder, vd), vd , sec = seccond , skipframe = True)
                        #---------------------------------------------------------- Create LIST
                        MARINE_MAMMAL = ranges_remove_duplicate(MARINE_MAMMAL,norm)
                        TURTLE = ranges_remove_duplicate(TURTLE,norm)
                        RAY = ranges_remove_duplicate(RAY,norm)
                        SHARK = ranges_remove_duplicate(SHARK,norm)
                        FISH = ranges_remove_duplicate(FISH,norm)
                        #---------------------------------------------------------- CUT VIDEO
                        window["-TOUT-"].update("CUTTING VIDEO...")
                        window['-MULTILINE-'].update(window['-MULTILINE-'].get() + "CUTTING VIDEO...")
                        child = outfol+ "/" + vd.replace('.mp4','') 
                        os.mkdir(child)
                        cut(MARINE_MAMMAL,os.path.join(folder, vd),"MARINE_MAMMAL",child)
                        cut(TURTLE,os.path.join(folder, vd),"TURTLE",child)
                        cut(RAY,os.path.join(folder, vd),"RAY",child)
                        cut(SHARK,os.path.join(folder, vd),"SHARK",child)
                        cut(FISH,os.path.join(folder, vd),"FISH",child)
                        
                    window['-MULTILINE-'].update(window['-MULTILINE-'].get() + "Completed\n")
                window["-TOUT-"].update("Completed")
            except Exception as e:
                print(e)
                window['-MULTILINE-'].update(e,text_color = 'Red')

window.close()
